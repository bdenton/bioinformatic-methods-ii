a. What source motif databases does the CDD incorporate?
Abbreviation	Database Name	Description
SMART	Simple Modular Architecture Research Tool	SMART is a web tool for the identification and annotation of protein domains, and provides a platform for the comparative study of complex domain architectures in genes and proteins. SMART is maintained by Chris Ponting, Peer Bork and colleagues, mainly at the EMBL Heidelberg. CDD contains a large fraction of the SMART collection.
Pfam	Protein families	Pfam is a large collection of multiple sequence alignments and hidden Markov models covering many common protein domains and families. Pfam is maintained by Alex Bateman and colleagues, mainly at the Wellcome Trust Sanger Institute. CDD contains a large fraction of the Pfam collection.
COGs	Clusters of Orthologous Groups of proteins	COGs is an NCBI-curated protein classification resource. Sequence alignments corresponding to COGs are created automatically from constituent sequences and have not been validated manually when imported into CDD.
TIGRFAM	The Institute for Genomic Research's database of protein families	TIGRFAM, a research project of the J. Craig Venter Institute, is a collection of manually curated protein families from The Institute for Genomic Research and consists of hidden Markov models (HMMs), multiple sequence alignments, Gene Ontology (GO) terminology, cross-references to related models in TIGRFAM and other databases, and pointers to literature.
PRK	PRotein K(c)lusters	Protein Clusters is an NCBI collection of related protein sequences (clusters) consisting of Reference Sequence proteins encoded by complete prokaryotic and chloroplast plasmids and genomes. It includes both curated and non-curated (automatically generated) clusters.

b. Comment on the size of the BRCA2 protein. 

3467 amino acids long


c. How many distinct protein domains does the BRCA2 protein possess (this is a bit tricky to
assess, but go by the number of unique hits in List of Domain Hits from the Specific Hits part of
the graphic – mousing over the Specific Hits symbols will helpfully highlight entries in the List of
Domain Hits)?

4

d. How many BRCA2 repeat domains (also called BRC repeats) does BRCA2 possess?

7

e. How many eukaryotic species (that is, the taxonomy span should be Cellular Organisms, or at
least Eukaryota) possess a BRCA2-like region containing OB1, OB2, OB3 (denoted as RPA 2b
in CDART) but lack BRC repeats (denoted in CDART as BRC2)?

8

f. What does this suggest about these domains? Could they function independently of one
another?

Yes, there are eight species that possess one domain, but not the other.

